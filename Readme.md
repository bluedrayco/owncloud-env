# Intention
Automate owncloud instalation process using vagrant

## Requirements

- [Vagrant](https://www.vagrantup.com/downloads)
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

# Instalation

The instalation process takes a few minutes, open your terminal and type:

```sh
vagrant up
```

After that the owncloud will be on **http://localhost:8080**

# Uninstall

The uninstall process is very simple:

```sh
vagrant destroy -f
```