#!/usr/bin/env bash


DB_PASSWORD=pass 
DB_DATABASE=owncloud

apt-get update

apt-get install -y apache2

sed -i "s/Options Indexes FollowSymLinks/Options FollowSymLinks/" /etc/apache2/apache2.conf

systemctl stop apache2.service
systemctl start apache2.service
systemctl enable apache2.service



debconf-set-selections <<< "mysql-server mysql-server/root_password password $DB_PASSWORD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DB_PASSWORD"

apt-get install -y mysql-server 
apt-get install -y mysql-client

apt-get install software-properties-common
add-apt-repository ppa:ondrej/php

apt-get update

apt-get install -y unzip php7.2 libapache2-mod-php7.2 php7.2-common php7.2-mbstring php7.2-xmlrpc php7.2-soap php7.2-apcu php7.2-smbclient php7.2-ldap php7.2-redis php7.2-gd php7.2-xml php7.2-intl php7.2-json php7.2-imagick php7.2-mysql php7.2-cli php7.2-mcrypt php7.2-ldap php7.2-zip php7.2-curl


mysql -u root -p"$DB_PASSWORD" -e "CREATE DATABASE $DB_DATABASE;"

cp /vagrant/.provision/php.ini /etc/php/7.2/apache2/php.ini

cd /tmp && wget https://download.owncloud.org/community/owncloud-10.6.0.zip
unzip owncloud-10.6.0.zip
sudo mv owncloud /var/www/html/owncloud/

cp /vagrant/.provision/000-default.conf /etc/apache2/sites-available/000-default.conf
a2dissite 000-default.conf 
systemctl reload apache2
a2ensite 000-default.conf 
systemctl reload apache2

a2enmod rewrite
a2enmod headers
a2enmod env
a2enmod dir
a2enmod mime

systemctl restart apache2.service


chown -R www-data /var/www/html